% 12/14/2021 Makoto. Modified. Switched to use pop_leadfield() to fix the gray brain issue. Inserting "cfg.grid.resolution = 10;" before pop_leadfield line 275 "EEG.dipfit.sourcemodel = ft_prepare_leadfield(cfg);" replicated the issue.
% 12/12/2021 Makoto. Created.

clear global
clear
close all
clc

set(0,'showhiddenhandles','on') % Make the GUI figure handle visible

% Launch EEGLAB without Fieldtrip Light.
addpath('/data/projects/makoto/git/lapwraploreta')

% Launch Fieldtrip
addpath('/data/projects/Greg/harmonics/code/fieldtrip-20210807')
ft_defaults

% Generate a dummy EEG data set with 346 channels.
chanlocs1005 = readlocs('/data/projects/makoto/Tools/eeglab2021.1/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
chanlocs1005 = chanlocs1005(4:end);
EEG = eeg_emptyset;
EEG.nbchan = length(chanlocs1005);
EEG.trials = 1;
EEG.pnts = 1000;
EEG.srate = 1000;
EEG.xmin = 1;
EEG.xmax = 1000;
EEG.times = 1:1000;
EEG.data = zeros(length(chanlocs1005), 1000);
EEG.chanlocs = chanlocs1005;
EEG = pop_chanedit(EEG, 'nosedir','+Y');

% Populate dipfit info.
EEG2 = pop_loadset('filename', 'G00006_Sz_Gamma.set', 'filepath', '/data/qumulo/Daisuke/fromDataProjects/BigData/p0200_upToDipfit/', 'loadmode', 'info');
EEG.dipfit = EEG2.dipfit;
EEG.icaweights = eye(EEG.nbchan);
EEG.icasphere  = eye(EEG.nbchan);
EEG.icawinv    = eye(EEG.nbchan);
EEG = eeg_checkset(EEG, 'ica');

EEGorig = EEG;

% Loop across different electrode densities.
for elecDensityIdx = 1:6
    
    % Retrieve the original data.
    EEG = EEGorig;
    
    elecLabels = {EEG.chanlocs.labels}';
    switch elecDensityIdx
        case 1 % 21 channels (10-20 system).
            electrodesUsed = {'Fp1','Fpz','Fp2',...
                              'F7','F3','Fz','F4','F8',...
                              'T7','C3','Cz','C4','T8',...
                              'P7','P3','Pz','P4','P8',...
                              'O1','Oz','O2'};
            [~, selectingIdx] = intersect(elecLabels, electrodesUsed);
            
        case 2
            % 31 channels.
            electrodesUsed = {'Fp1','Fpz','Fp2',...
                            'F7','F3','Fz','F4','F8',...
                            'FT7','FC3','FCz','FC4','FT8',...
                            'T7','C3','Cz','C4','T8',...
                            'TP7','CP3','CPz','CP4','TP8',...
                            'P7', 'P3','Pz','P4','P8',...
                            'O1','Oz','O2'};
            [~, selectingIdx] = intersect(elecLabels, electrodesUsed);
            
        case 3             
            % 64 channels.
            electrodesUsed = {'Fp1','Fpz','Fp2',...
                            'AF3','AF4',...
                            'F7','F5','F3','F1','Fz','F2','F4','F6','F8',...
                            'FT9','FT7','FC5','FC3','FC1','FCz','FC2','FC4','FC6','FT8','FT10',...
                            'T7','C5','C3','C1','Cz','C2','C4','C6','T8',...
                            'TP9','TP7','CP5','CP3','CP1','CPz','CP2','CP4','CP6','TP8','TP10',...
                            'P7','P5','P3','P1','Pz','P2','P4','P6','P8',...
                            'PO7','PO5','PO3','POz','PO4','PO6','PO8',...
                            'O1','Oz','O2'};
            [~, selectingIdx] = intersect(elecLabels, electrodesUsed);
            
        case 4 % 84 channels (10-10 system without Nz).
            electrodesUsed = {'Nz',...
                              'Fp1','Fpz','Fp2',...
                              'AF7','AF5','AF3','AF1','AFz','AF2','AF4','AF6','AF8',...
                              'F9','F7','F5','F3','F1','Fz','F2','F4','F6','F8','F10',...
                              'FT9','FT7','FC5','FC3','FC1','FCz','FC2','FC4','FC6','FT8','FT10',...
                              'T9','T7','C5','C3','C1','Cz','C2','C4','C6','T8','T10',...
                              'TP9','TP7','CP5','CP3','CP1','CPz','CP2','CP4','CP6','TP8','TP10',...
                              'P9','P7','P5','P3','P1','Pz','P2','P4','P6','P8','P10',...
                              'PO9','PO7','PO5','PO3','PO1','POz','PO2','PO4','PO6','PO8','PO10',...
                              'O1','Oz','O2',...
                              'I1','Iz','I2'};
            [~, selectingIdx] = intersect(elecLabels, electrodesUsed);
            
        case 5 % 141 channels (10-5 system without Nz).
            electrodesUsed = {'Nz',...
                              'Fp1','Fpz','Fp2',...
                              'AFp3','AFp4',...
                              'AF7','AF5h','AF3h','AFz','AF4h','AF6h','AF8',...
                              'AFF7h','AFF5h','AFF3h','AFF1h','AFF2h','AFF4h','AFF6h','AFF8h',...                              
                              'F9','F7','F5','F3','F1','Fz','F2','F4','F6','F8','F10',...
                              'FFT9h','FFT7h','FFC5h','FFC1h','FFC2h','FFC4h','FFC6h','FFT8h','FFT10h',...
                              'FT9','FT7','FC5','FC3','FC1','FCz','FC2','FC4','FC6','FT8','FT10',...
                              'FTT9h','FTT7h','FCC5h','FCC1h','FCC2h','FCC4h','FCC6h','FTT8h','FTT10h',...
                              'T9','T7','C5','C3','C1','Cz','C2','C4','C6','T8','T10',...
                              'TTP7h','CCP5h','CCP3h','CCP1h','CCP2h','CCP4h','CCP6h','TTP8h',...
                              'TP7','CP5','CP3','CP1','CPz','CP2','CP4','CP6','TP8',...
                              'TPP9h','TPP7h','CPP5h','CPP3h','CPP1h','CPP2h','CPP4h','CPP6h','TPP8h','TPP10h',...
                              'P9','P7','P5','P3','P1','Pz','P2','P4','P6','P8','P10',...
                              'PPO9h','PPO7h','PPO5h','PPO3h','PPO1h','PPO2h','PPO4h','PPO6h','PPO8h','PPO10h',...
                              'PO9','PO7','PO5h','PO3h','POz','PO4h','PO6h','PO8','PO10',...
                              'POO9h','O1','POO3','Oz','POO4','O2','POO10h',...
                              'I1','O1h','Iz','O2h','I2'};
            [~, selectingIdx] = intersect(elecLabels, electrodesUsed);
            
        case 6 % 343 channels (use all)
            selectingIdx = 1:length(chanlocs1005);
            
    end
    
    % Perform electrode selection.
    EEG = pop_select(EEG, 'channel', selectingIdx);
        
    % Perform EEGLAB's leadfield calculation.
    EEG = pop_leadfield(EEG, 'sourcemodel', 'LORETA-Talairach-BAs.mat','sourcemodel2mni',[],'downsample',1);
    
    %%%%%%%%%%%%%%%%%%%%%%%
    %%% LORETA package. %%%
    %%%%%%%%%%%%%%%%%%%%%%%
    % Load headmodel.
    %headmodelPath = '/data/projects/makoto/Tools/eeglab2021.1/plugins/dipfit/standard_BEM/standard_vol.mat';
    %headmodelPath = EEG.dipfit.hdmfile.
    %headmodel = load('-mat', headmodelPath);
    headmodel = load('-mat', EEG.dipfit.hdmfile);
    headmodel = headmodel.vol;
    
    % Load MRI.
    %mriPath = '/data/projects/makoto/Tools/eeglab2021.1/plugins/dipfit/standard_BEM/standard_mri.mat';
    %mri = load('-mat', mriPath);
    mri = load('-mat', EEG.dipfit.mrifile);
    mri = ft_volumereslice([], mri.mri);
    
    % Precomputed gridOutput.
    %gridOutput = calc_gridOutput(EEG, headmodel, mri);
    %gridOutput = EEG.dipfit.sourcemodel;
    
    % To turn off visualization, go to this file line 8 cfg.visible = 'yes';
    % /data/projects/Greg/harmonics/code/fieldtrip-20210807/private/open_figure
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Create the triple plots. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    set(groot, 'defaulttextinterpreter','tex');
    set(groot, 'defaultAxesTickLabelInterpreter','tex');
    set(groot, 'defaultLegendInterpreter','tex');
    
    figHandle = figure;
    set(figHandle, 'position', [400 400 1000 900])
    
    tiledLayoutHandles = tiledlayout(figHandle, 2, 2);
    nexttile
    ax1 = gca;
    nexttile
    ax2 = gca;
    axis off
    nexttile
    ax3 = gca;
    axis off
    nexttile
    ax4 = gca;
    axis off
    
    % Plot all channel location.
    chaninfo.plotrad = [];
    chaninfo.shrink  = [];
    chaninfo.nosedir = '+X';
    chaninfo.nodatchans = [];
    chaninfo.icachansind = EEG.icachansind;
    chaninfo.filename = EEG.dipfit.chanfile;
    
    % Find Fz.
    fzIdx = find(strcmp({EEG.chanlocs.labels}', 'Fz'));
    
    for chIdx = fzIdx
        
        currentData = zeros(EEG.nbchan,1);
        currentData(chIdx) = 1;
        EEG.data = repmat(currentData, [1 EEG.pnts]);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Plot channnel location on the 2-D scalp map. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        axes(ax1)
        topoplot([], EEG.chanlocs, 'style', 'blank', 'drawaxis', 'on', 'electrodes', ...
            'labelpoint', 'plotrad', [], 'chaninfo', chaninfo, 'plotchans', chIdx, 'emarker', {'.','r',[],1});
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Plot eLORETA images. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %calc_eLoreta(EEG, 0, [], gridOutput, headmodel, mri);
        calc_eLoreta(EEG, 0, [], EEG.dipfit.sourcemodel, headmodel, mri);
        
        loretaFigureHandle1 = gcf;
        loretaAxesHandle1   = gca;
        
        % Copy eLORETA handles.
        % Taken from https://www.mathworks.com/matlabcentral/answers/93226-how-do-i-copy-the-axes-from-an-existing-gui-into-a-new-figure-so-that-the-new-figure-does-not-have-a
        axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
        loretaFigureHandle2 = figure; % Open a new figure with handle f1
        copyingAxesHandle2  = copyobj(axesHandles, loretaFigureHandle2); % Copy axes object h into figure f1
        loretaAxesHandle2   = gca;
        
        axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
        loretaFigureHandle3 = figure; % Open a new figure with handle f1
        copyingAxesHandle3  = copyobj(axesHandles, loretaFigureHandle3); % Copy axes object h into figure f1
        loretaAxesHandle3   = gca;
        
        loretaAxesHandle1.Parent = tiledLayoutHandles;
        loretaAxesHandle1.Layout.Tile = 2;
        close(loretaFigureHandle1)
        axes(loretaAxesHandle1)
        view([130 35])
        colormap('jet')
        delete(findall(gca,'Type','light'))
        camlight('headlight')
        
        loretaAxesHandle2.Parent = tiledLayoutHandles;
        loretaAxesHandle2.Layout.Tile = 3;
        close(loretaFigureHandle2)
        axes(loretaAxesHandle2)
        view([-130 35])
        colormap('jet')
        delete(findall(gca,'Type','light'))
        camlight('headlight')
        
        loretaAxesHandle3.Parent = tiledLayoutHandles;
        loretaAxesHandle3.Layout.Tile = 4;
        close(loretaFigureHandle3)
        axes(loretaAxesHandle3)
        view([0 35])
        colormap('jet')
        delete(findall(gca,'Type','light'))
        camlight('headlight')
        axis off
        
        % Print.
        print(sprintf('/data/mobi/Yash/p0613_singleChannelEloreta_1005/%s_1005_%d', EEG.chanlocs(chIdx).labels, elecDensityIdx), '-djpeg95', '-r150')
        
        % Clear the axes.
        cla(ax1)
        cla(ax2)
        cla(ax3)
        cla(ax4)
        cla(loretaAxesHandle1)
        cla(loretaAxesHandle2)
        cla(loretaAxesHandle3)
    end
end