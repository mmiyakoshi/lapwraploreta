% The 5th input 'gridOutput' should be calculated using calc_gridOutput.
%
% 11/21/2021 Makoto. Modified. Changed to eeglab2fieldtrip(EEG, 'preprocessing', 'dipfit')
% 11/03/2021 Makoto. Modified. sourcePlotFunctional.pow = sourcePlotPow1; ft_sourceplot(sourcePlotCfg, sourcePlotFunctional);
% 08/18/2021 Makoto. Modified.
% 10/08/2019 Makoto. Created.

function [sourcePlotCfg, sourcePlotFunctional, sourcePlotPow1, sourcePlotPow2] = ...
                          calc_eLoretaSubtraction(EEG1, EEG2, latencyS, minMaxColor, gridOutput, headmodel, mri)

%% Copying parameters.
params1 = {'method' 'eloreta'};
    % params2 = {'method' 'slice'};
    % params2 = {'method' 'ortho'};
params2 = {'method' 'surface'};
    % params2 = {'method' 'glassbrain'}; % Not usable.
    % params2 = {'method' 'vertex'};     % Not usable.
    % params2 = {'method' 'cloud'};      % Not usable.
options = { 'ft_sourceanalysis_params' params1 'ft_sourceplot_params' params2 };

g = finputcheck(options, { 'ft_sourceanalysis_params'  'cell'    {}         { 'method' 'eloreta' };
                           'ft_sourceplot_params'      'cell'    []         { 'method' 'slice' } }, 'pop_dipfit_loreta');

%% Prepare the right input structure.

% Convert EEG to Fieldtrip.
%dataPre = eeglab2fieldtrip(EEG, 'raw');
% dataPre1 = eeglab2fieldtrip(EEG1, 'timelock');
% dataPre2 = eeglab2fieldtrip(EEG2, 'timelock');
dataPre1 = eeglab2fieldtrip(EEG1, 'preprocessing', 'dipfit'); % does the transformation (Taken from pop_leadfield() line 240)
dataPre2 = eeglab2fieldtrip(EEG2, 'preprocessing', 'dipfit'); % does the transformation (Taken from pop_leadfield() line 240)

    %{
    figure
    plot(dataPre1.avg')
    figure
    plot(dataPre1.var)
    %}

% Select the latency to show cortical topo.
[errorMs, latencyIdx] = min(abs(EEG1.times - latencyS));

% Build input data structure.
freqPre1.label  = {EEG1.chanlocs.labels}; 
freqPre1.dimord = 'chan_freq'; 
freqPre1.freq   = 10; % most likely this is dummy number. Needs to be checked.
freqPre1.powspctrm = mean(EEG1.data(:,latencyIdx,:),3).*mean(EEG1.data(:,latencyIdx,:),3);
freqPre1.crsspctrm = mean(EEG1.data(:,latencyIdx,:),3)*mean(EEG1.data( :,latencyIdx,:),3)';
freqPre1.elec      = dataPre1.elec;

freqPre2.label  = {EEG2.chanlocs.labels}; 
freqPre2.dimord = 'chan_freq'; 
freqPre2.freq   = 10; % most likely this is dummy number. Needs to be checked.
freqPre2.powspctrm = mean(EEG2.data(:,latencyIdx,:),3).*mean(EEG2.data(:,latencyIdx,:),3);
freqPre2.crsspctrm = mean(EEG2.data(:,latencyIdx,:),3)*mean(EEG2.data( :,latencyIdx,:),3)';
freqPre2.elec      = dataPre2.elec;


%% Read headmodel
% If headmodel is not precomputed, calculate it now.
if isempty(headmodel)
    headmodel = load('-mat', EEG1.dipfit.hdmfile);
    headmodel = headmodel.vol;
end

%% prepare leadfield matrix
cfg                 = [];
cfg.elec            = freqPre1.elec;
cfg.headmodel       = headmodel;
cfg.reducerank      = 2;
cfg.grid.resolution = 10;   % use a 3-D grid with a 10 mm resolution
cfg.grid.unit       = 'mm';
cfg.channel         = { 'all' };

% If gridOutput is not precomputed, calculate it now.
if isempty(gridOutput)
    [gridOutput] = ft_prepare_leadfield(cfg);
    %save('I:\Iman\p0002_loretaPractice\gridOutput', 'gridOutput')
    %load I:\Iman\code\p9010gridOutput
end

% source localization
cfg              = struct(g.ft_sourceanalysis_params{:}); 
cfg.frequency    = []; % 18
cfg.grid         = gridOutput; 
cfg.headmodel    = headmodel;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = 0;

% Obtain the target frame.
% [errorMs, latencyIdx] = min(abs(EEG.times - latencyMs));
% freqPre.powspctrm = mean(EEG.data(:,latencyIdx,:),3).*mean(EEG.data(:,latencyIdx,:),3);
% freqPre.crsspctrm = mean(EEG.data(:,latencyIdx,:),3)*mean(EEG.data( :,latencyIdx,:),3)';

% Perform LORETA.
sourcePost_nocon1 = ft_sourceanalysis(cfg, freqPre1);
sourcePost_nocon2 = ft_sourceanalysis(cfg, freqPre2);


% If mri is not precomputed, calculate it now.
if isempty(mri)
    mri = load('-mat', EEG1.dipfit.mrifile);
    mri = ft_volumereslice([], mri.mri);
end
cfg2            = [];
cfg2.downsample = 2;
cfg2.parameter = 'avg.pow';
sourcePost_nocon1.oridimord = 'pos';
sourcePost_nocon1.momdimord = 'pos';
sourcePostInt_nocon1  = ft_sourceinterpolate(cfg2, sourcePost_nocon1 , mri);
sourcePostInt_nocon2  = ft_sourceinterpolate(cfg2, sourcePost_nocon2 , mri);

% Convert to single to save disk space
sourcePostInt_nocon3     = sourcePostInt_nocon1;
sourcePostInt_nocon3.pow = sourcePostInt_nocon1.pow-sourcePostInt_nocon2.pow;

% Plot.
cfg2              = struct(g.ft_sourceplot_params{:});
cfg2.funparameter = 'avg.pow';
cfg2.funcolorlim  = minMaxColor; 
ft_sourceplot(cfg2, sourcePostInt_nocon3);
colormap('jet')

% Output
sourcePlotCfg        = cfg2;
sourcePlotFunctional = sourcePostInt_nocon1;
sourcePlotFunctional.pow = [];
sourcePlotPow1       = single(sourcePostInt_nocon1.pow);
sourcePlotPow2       = single(sourcePostInt_nocon2.pow);