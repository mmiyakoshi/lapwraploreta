% 12/17/2021 Makoto. Modified.
% 12/09/2021 Makoto. Used.
% 11/26/2021 Makoto. Used.
% 10/07/2021 Makoto. Used. Baseline corrected.

clear
close all
clc

% Launch EEGLAB without Fieldtrip Light.
addpath('/data/projects/makoto/git/lapwraploreta')

% Launch Fieldtrip
addpath('/data/projects/Greg/harmonics/code/fieldtrip-20210807')
ft_defaults

clear
clc

% Load ACB list.
%antiCholinergicBurdenList = readtable('/data/mobi/Yash/p0501_newAcbListFromYash/TRANS_8-23-21_1192021_CSV.csv');
antiCholinergicBurdenList = readtable('/data/mobi/Yash/p0601_anotherAcbListFromYash/ACB_TRANS_database_11292021_edited.csv');

% Load precalculated ERP data.
load('/data/mobi/Yash/p0500_computeErpWithCt/mmnAndAssr')

% Remove zero data from the imported data.
nonzeroDataIdx = find(squeeze(std(std(standardErp,0,1),0,2))~=0);
assr20Erp = assr20Erp(:,:,nonzeroDataIdx);
assr30Erp = assr30Erp(:,:,nonzeroDataIdx);
assr40Erp = assr40Erp(:,:,nonzeroDataIdx);
standardErp = standardErp(:,:,nonzeroDataIdx);
devientErp  = devientErp( :,:,nonzeroDataIdx);
goodSubjNameList = goodSubjNameList(nonzeroDataIdx);
groupNameList    = groupNameList(nonzeroDataIdx);

acbSubjNameList = table2cell(antiCholinergicBurdenList(:,2));
acbNumberList   = zeros(length(goodSubjNameList),1);
for goodSubjIdx = 1:length(goodSubjNameList)
    
    currentSubjName = goodSubjNameList(goodSubjIdx);
    hitIdx = find(contains(acbSubjNameList, currentSubjName));
    if isempty(hitIdx)
        continue
    else
        acbNumberList(goodSubjIdx) = table2array(antiCholinergicBurdenList(hitIdx,3));
    end
end

% Load dummy EEG data.
EEG = pop_loadset('filename', 'G00006_Sz_Gamma.set', 'filepath', '/data/qumulo/Daisuke/fromDataProjects/BigData/p0200_upToDipfit/', 'loadmode', 'info');

% Perform EEGLAB's leadfield calculation.
EEG = pop_leadfield(EEG, 'sourcemodel', 'LORETA-Talairach-BAs.mat','sourcemodel2mni',[],'downsample',1);
  

%% Movie (eLORETA)

% Obtain Dev-Sed.
mmnDiffTensor = devientErp-standardErp;

% Apply baseline correction.
baselineIdx = find(erpTime>=-100 & erpTime<=0);
mmnDiffTensor_baselineCorrected = bsxfun(@minus, mmnDiffTensor, mean(mmnDiffTensor(:,baselineIdx,:),2));

% Separate groups.
%ctIdx = find(strcmp(groupNameList, 'Ct'));
ctIdx = find(strcmp(groupNameList, 'Sz'));

ctDiffErp = mmnDiffTensor(:,:,ctIdx);

% Prepare dummy EEG for eLoreta.
EEG.times = erpTime;
EEG.data = double(mean(ctDiffErp,3));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prepare video output parameters. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
outputVideo = VideoWriter('/data/mobi/Yash/p0614_eLoretaDemo2/movie', 'Motion JPEG AVI');
outputVideo.FrameRate = 5;
outputVideo.Quality   = 95;
open(outputVideo)


%%%%%%%%%%%%%%%%%%%%%%%
%%% LORETA package. %%%
%%%%%%%%%%%%%%%%%%%%%%%
% Load headmodel.
headmodel = load('-mat', EEG.dipfit.hdmfile);
headmodel = headmodel.vol;

% Load MRI.
mri = load('-mat', EEG.dipfit.mrifile);
mri = ft_volumereslice([], mri.mri);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create the triple plots. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(groot, 'defaulttextinterpreter','tex');  
set(groot, 'defaultAxesTickLabelInterpreter','tex');  
set(groot, 'defaultLegendInterpreter','tex');

figHandle = figure;
set(figHandle, 'position', [300 300 1000 900])

% Plot Fz ERP.
erpPlotTimeIdx = find(erpTime>=0 & erpTime <= 500);

tiledLayoutHandles = tiledlayout(figHandle, 2, 2);
nexttile
ax1 = gca;
erp1Handle = plot(erpTime(erpPlotTimeIdx), mean(ctDiffErp(9,erpPlotTimeIdx,:),3),   'linewidth', 2);

xlabel('Latency (ms)')
ylabel('Amplitude (\muV)')
xlim([0 500])
line(xlim, [0 0], 'color', [0 0 0])

nexttile
ax2 = gca;
axis off
nexttile
ax3 = gca;
axis off
nexttile
ax4 = gca;
axis off

powMatrix = single(zeros(128^3, length(erpPlotTimeIdx)));
for timeIdx = 1:length(erpPlotTimeIdx)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Move the vertical line. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    axes(ax1)
    lineHandle = line([erpTime(erpPlotTimeIdx(timeIdx)) erpTime(erpPlotTimeIdx(timeIdx))], ylim, 'color', [0 0 0], 'linestyle', ':');
    title(sprintf('Fz ERP, Dev-Std %d ms', erpTime(erpPlotTimeIdx(timeIdx))))
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Plot eLORETA images. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [sourcePlotCfg, sourcePlotFunctional] = calc_eLoreta(EEG, 188, [-1500 1500], EEG.dipfit.sourcemodel, headmodel, mri);

    
    [sourcePlotCfg, sourcePlotFunctional] = calc_eLoreta(EEG, erpTime(erpPlotTimeIdx(timeIdx)), [-1500 1500], EEG.dipfit.sourcemodel, headmodel, mri);
    powMatrix(:,timeIdx) = sourcePlotFunctional.pow;
    loretaFigureHandle1 = gcf;
    loretaAxesHandle1   = gca;
        
    % Copy eLORETA handles.
    % Taken from https://www.mathworks.com/matlabcentral/answers/93226-how-do-i-copy-the-axes-from-an-existing-gui-into-a-new-figure-so-that-the-new-figure-does-not-have-a
    axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
    loretaFigureHandle2 = figure; % Open a new figure with handle f1
    copyingAxesHandle2  = copyobj(axesHandles, loretaFigureHandle2); % Copy axes object h into figure f1
    loretaAxesHandle2   = gca;
    
    axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
    loretaFigureHandle3 = figure; % Open a new figure with handle f1
    copyingAxesHandle3  = copyobj(axesHandles, loretaFigureHandle3); % Copy axes object h into figure f1
    loretaAxesHandle3   = gca;
    
    loretaAxesHandle1.Parent = tiledLayoutHandles;
    loretaAxesHandle1.Layout.Tile = 2;
    close(loretaFigureHandle1)
    axes(loretaAxesHandle1)
    colorbar
    view([140 10])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')
    
    loretaAxesHandle2.Parent = tiledLayoutHandles;
    loretaAxesHandle2.Layout.Tile = 3;
    close(loretaFigureHandle2)
    axes(loretaAxesHandle2)
    view([-140 10])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')
    
    loretaAxesHandle3.Parent = tiledLayoutHandles;
    loretaAxesHandle3.Layout.Tile = 4;
    close(loretaFigureHandle3)
    axes(loretaAxesHandle3)
    view([0 35])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')

    % Write the current figure to a frame of the video.
    currentMovieFrame = getframe(figHandle);
    writeVideo(outputVideo, currentMovieFrame);
    
    % Clear the axes.
    delete(lineHandle)
    cla(ax2)
    cla(ax3)
    cla(ax4)
    cla(loretaAxesHandle1)
    cla(loretaAxesHandle2)
    cla(loretaAxesHandle3)
end

close(outputVideo)
save('/data/mobi/Yash/p0614_eLoretaDemo2/powMatrix', 'powMatrix', '-v7.3');