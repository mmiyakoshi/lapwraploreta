% 12/14/2021 Makoto. Modified. Switched to use pop_leadfield() to fix the gray brain issue. Inserting "cfg.grid.resolution = 10;" before pop_leadfield line 275 "EEG.dipfit.sourcemodel = ft_prepare_leadfield(cfg);" replicated the issue.
% 12/12/2021 Makoto. Created.

clear
close all
clc

set(0,'showhiddenhandles','on') % Make the GUI figure handle visible

% Launch EEGLAB without Fieldtrip Light.
addpath('/data/projects/makoto/git/lapwraploreta')

% Launch Fieldtrip
addpath('/data/projects/Greg/harmonics/code/fieldtrip-20210807')
ft_defaults

% Load dummy EEG data.
%EEG = pop_loadset('filename', 'G00006_Sz_Gamma.set', 'filepath', '/data/qumulo/Daisuke/fromDataProjects/BigData/p0200_upToDipfit/', 'loadmode', 'info');
EEG = pop_loadset('filename', 'G00006_Sz_Gamma.set', 'filepath', '/data/qumulo/Daisuke/fromDataProjects/BigData/p0200_upToDipfit/');

tStart = tic;

% Perform EEGLAB's leadfield calculation.
EEG = pop_leadfield(EEG, 'sourcemodel', 'LORETA-Talairach-BAs.mat','sourcemodel2mni',[],'downsample',1);

%%%%%%%%%%%%%%%%%%%%%%%
%%% LORETA package. %%%
%%%%%%%%%%%%%%%%%%%%%%%
% Load headmodel.
%headmodelPath = '/data/projects/makoto/Tools/eeglab2021.1/plugins/dipfit/standard_BEM/standard_vol.mat';
%headmodelPath = EEG.dipfit.hdmfile.
%headmodel = load('-mat', headmodelPath);
headmodel = load('-mat', EEG.dipfit.hdmfile);
headmodel = headmodel.vol;

% Load MRI.
%mriPath = '/data/projects/makoto/Tools/eeglab2021.1/plugins/dipfit/standard_BEM/standard_mri.mat';
%mri = load('-mat', mriPath);
mri = load('-mat', EEG.dipfit.mrifile);
mri = ft_volumereslice([], mri.mri);

% Precomputed gridOutput.
%gridOutput = calc_gridOutput(EEG, headmodel, mri);
%gridOutput = EEG.dipfit.sourcemodel;

% To turn off visualization, go to this file line 8 cfg.visible = 'yes';
% /data/projects/Greg/harmonics/code/fieldtrip-20210807/private/open_figure

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create the triple plots. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(groot, 'defaulttextinterpreter','tex');  
set(groot, 'defaultAxesTickLabelInterpreter','tex');  
set(groot, 'defaultLegendInterpreter','tex');

figHandle = figure;
set(figHandle, 'position', [400 400 1000 900])

tiledLayoutHandles = tiledlayout(figHandle, 2, 2);
nexttile
ax1 = gca;
nexttile
ax2 = gca;
axis off
nexttile
ax3 = gca;
axis off
nexttile
ax4 = gca;
axis off

% Plot all channel location.
chaninfo.plotrad = [];
chaninfo.shrink  = [];
chaninfo.nosedir = '+X';
chaninfo.nodatchans = [];
chaninfo.icachansind = EEG.icachansind;
chaninfo.filename = EEG.dipfit.chanfile;

for chIdx = 1:EEG.nbchan
    
    currentData = zeros(EEG.nbchan,1);
    currentData(chIdx) = 1;
    EEG.data = repmat(currentData, [1 EEG.pnts]);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Plot channnel location on the 2-D scalp map. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    axes(ax1)
    topoplot([], EEG.chanlocs, 'style', 'blank', 'drawaxis', 'on', 'electrodes', ...
         'labelpoint', 'plotrad', [], 'chaninfo', chaninfo, 'plotchans', chIdx, 'emarker', {'.','r',[],1});
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Plot eLORETA images. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %calc_eLoreta(EEG, 0, [], gridOutput, headmodel, mri);
    calc_eLoreta(EEG, 0, [], EEG.dipfit.sourcemodel, headmodel, mri);
    
    loretaFigureHandle1 = gcf;
    loretaAxesHandle1   = gca;
    
    % Copy eLORETA handles.
    % Taken from https://www.mathworks.com/matlabcentral/answers/93226-how-do-i-copy-the-axes-from-an-existing-gui-into-a-new-figure-so-that-the-new-figure-does-not-have-a
    axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
    loretaFigureHandle2 = figure; % Open a new figure with handle f1
    copyingAxesHandle2  = copyobj(axesHandles, loretaFigureHandle2); % Copy axes object h into figure f1
    loretaAxesHandle2   = gca;
     
    axesHandles = findobj(loretaFigureHandle1,'type','axes'); % Find the axes object in the GUI
    loretaFigureHandle3 = figure; % Open a new figure with handle f1
    copyingAxesHandle3  = copyobj(axesHandles, loretaFigureHandle3); % Copy axes object h into figure f1
    loretaAxesHandle3   = gca;
    
    loretaAxesHandle1.Parent = tiledLayoutHandles;
    loretaAxesHandle1.Layout.Tile = 2;
    close(loretaFigureHandle1)
    axes(loretaAxesHandle1)
    view([130 35])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')
    
    loretaAxesHandle2.Parent = tiledLayoutHandles;
    loretaAxesHandle2.Layout.Tile = 3;
    close(loretaFigureHandle2)
    axes(loretaAxesHandle2)
    view([-130 35])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')
    
    loretaAxesHandle3.Parent = tiledLayoutHandles;
    loretaAxesHandle3.Layout.Tile = 4;
    close(loretaFigureHandle3)
    axes(loretaAxesHandle3)
    view([0 35])
    colormap('jet')
    delete(findall(gca,'Type','light'))
    camlight('headlight')
    axis off
    
    tElapsed = toc(tStart);
    
    % Print.
    print(sprintf('/data/mobi/Yash/p0612_singleChannelEloreta/%s', EEG.chanlocs(chIdx).labels), '-djpeg95', '-r150')
    
    % Clear the axes.
    cla(ax1)
    cla(ax2)
    cla(ax3)
    cla(ax4)
    cla(loretaAxesHandle1)
    cla(loretaAxesHandle2)
    cla(loretaAxesHandle3)
end