% If no precomputed, use EEG = calc_eLoreta(EEG, latencyMs, [], [], []).
%
% 12/14/2021 Makoto. The gray brain issue solved. Switch to use pop_leadfield().
% 11/21/2021 Makoto. Modified. Changed to eeglab2fieldtrip(EEG, 'preprocessing', 'dipfit')
% 08/18/2021 Makoto. Modified.
% 10/08/2019 Makoto. Created.

function gridOutput = calc_gridOutput(EEG, headmodel, mri)

%% Copying parameters.
params1 = {'method' 'eloreta'};
    % params2 = {'method' 'slice'};
    % params2 = {'method' 'ortho'};
params2 = {'method' 'surface'};
    % params2 = {'method' 'glassbrain'}; % Not usable.
    % params2 = {'method' 'vertex'};     % Not usable.
    % params2 = {'method' 'cloud'};      % Not usable.
options = { 'ft_sourceanalysis_params' params1 'ft_sourceplot_params' params2 };

g = finputcheck(options, { 'ft_sourceanalysis_params'  'cell'    {}         { 'method' 'eloreta' };
                           'ft_sourceplot_params'      'cell'    []         { 'method' 'slice' } }, 'pop_dipfit_loreta');

%% Prepare the right input structure.

% Convert EEG to Fieldtrip.
%dataPre = eeglab2fieldtrip(EEG, 'raw');
%dataPre = eeglab2fieldtrip(EEG, 'timelock');
dataPre = eeglab2fieldtrip(EEG, 'preprocessing', 'dipfit'); % does the transformation (Taken from pop_leadfield() line 240)


% Build input data structure.
freqPre.label  = {EEG.chanlocs.labels}; 
freqPre.dimord = 'chan_freq'; 
freqPre.freq   = 10; % most likely this is dummy number. Needs to be checked.
freqPre.powspctrm = mean(EEG.data(:,1,:),3).*mean(EEG.data(:,1,:),3);
freqPre.crsspctrm = mean(EEG.data(:,1,:),3)*mean(EEG.data( :,1,:),3)';
freqPre.elec      = dataPre.elec;

%% Read headmodel
% If headmodel is not precomputed, calculate it now.
if isempty(headmodel)
    headmodel = load('-mat', EEG.dipfit.hdmfile);
    headmodel = headmodel.vol;
end

%% prepare leadfield matrix
cfg                 = [];
cfg.elec            = freqPre.elec;
cfg.headmodel       = headmodel;
cfg.reducerank      = 2;
cfg.grid.resolution = 10; % use a 3-D grid with a 10 mm resolution. Use 5 mm to avoid the gray brain issue!
cfg.grid.unit       = 'mm';
cfg.channel         = { 'all' };

% Calculate gridOutput.
gridOutput = ft_prepare_leadfield(cfg);