% If no precomputed, use EEG = calc_eLoreta(EEG, latencyMs, [], [], []).
% The 4th input 'gridOutput' should be calculated using calc_gridOutput.
%
% 11/21/2021 Makoto. Modified. Changed to eeglab2fieldtrip(EEG, 'preprocessing', 'dipfit')
% 08/18/2021 Makoto. Modified.
% 10/08/2019 Makoto. Created.

function [sourcePlotCfg, sourcePlotFunctional] = calc_eLoreta(EEG, latencyS, minMaxColor, gridOutput, headmodel, mri)

%% Copying parameters.
params1 = {'method' 'eloreta'};
    % params2 = {'method' 'slice'};
    % params2 = {'method' 'ortho'};
params2 = {'method' 'surface'};
    % params2 = {'method' 'glassbrain'}; % Not usable.
    % params2 = {'method' 'vertex'};     % Not usable.
    % params2 = {'method' 'cloud'};      % Not usable.
options = { 'ft_sourceanalysis_params' params1 'ft_sourceplot_params' params2 };

g = finputcheck(options, { 'ft_sourceanalysis_params'  'cell'    {}         { 'method' 'eloreta' };
                           'ft_sourceplot_params'      'cell'    []         { 'method' 'slice' } }, 'pop_dipfit_loreta');

%% Prepare the right input structure.

% Convert EEG to Fieldtrip.
%dataPre = eeglab2fieldtrip(EEG, 'raw');
%dataPre = eeglab2fieldtrip(EEG, 'timelock');
dataPre = eeglab2fieldtrip(EEG, 'preprocessing', 'dipfit'); % does the transformation (Taken from pop_leadfield() line 240)

% Select the latency to show cortical topo.
[errorMs, latencyIdx] = min(abs(EEG.times - latencyS));

% Build input data structure.
freqPre.label  = {EEG.chanlocs.labels}; 
freqPre.dimord = 'chan_freq'; 
freqPre.freq   = 10; % most likely this is dummy number. Needs to be checked.
freqPre.powspctrm = mean(EEG.data(:,latencyIdx,:),3).*mean(EEG.data(:,latencyIdx,:),3);
freqPre.crsspctrm = mean(EEG.data(:,latencyIdx,:),3)*mean(EEG.data( :,latencyIdx,:),3)';
freqPre.elec      = dataPre.elec;

%% Read headmodel
% If headmodel is not precomputed, calculate it now.
if isempty(headmodel)
    headmodel = load('-mat', EEG.dipfit.hdmfile);
    headmodel = headmodel.vol;
end

%% prepare leadfield matrix
cfg                 = [];
cfg.elec            = freqPre.elec;
cfg.headmodel       = headmodel;
cfg.reducerank      = 2;
cfg.grid.resolution = 10;   % use a 3-D grid with a 10 mm resolution
cfg.grid.unit       = 'mm';
cfg.channel         = { 'all' };

% % If gridOutput is not precomputed, calculate it now.
% if isempty(gridOutput)
%     [gridOutput] = ft_prepare_leadfield(cfg);
%     save('I:\Iman\p0002_loretaPractice\gridOutput', 'gridOutput')
% end

% source localization
cfg              = struct(g.ft_sourceanalysis_params{:}); 
cfg.frequency    = []; % 18
cfg.grid         = gridOutput; 
cfg.headmodel    = headmodel;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = 0;

% Obtain the target frame.
% [errorMs, latencyIdx] = min(abs(EEG.times - latencyMs));
% freqPre.powspctrm = mean(EEG.data(:,latencyIdx,:),3).*mean(EEG.data(:,latencyIdx,:),3);
% freqPre.crsspctrm = mean(EEG.data(:,latencyIdx,:),3)*mean(EEG.data( :,latencyIdx,:),3)';

% Perform LORETA.
sourcePost_nocon = ft_sourceanalysis(cfg, freqPre);

% If mri is not precomputed, calculate it now.
if isempty(mri)
    mri = load('-mat', EEG.dipfit.mrifile);
    mri = ft_volumereslice([], mri.mri);
end
cfg2            = [];
cfg2.downsample = 2;
cfg2.parameter = 'avg.pow';
sourcePost_nocon.oridimord = 'pos';
sourcePost_nocon.momdimord = 'pos';
sourcePostInt_nocon  = ft_sourceinterpolate(cfg2, sourcePost_nocon , mri);

% Convert to single to save disk space
sourcePostInt_nocon.anatomy = single(sourcePostInt_nocon.anatomy);
sourcePostInt_nocon.pow     = single(sourcePostInt_nocon.pow);
sourcePostInt_nocon.pos     = single(sourcePostInt_nocon.pos);

% Plot.
cfg2              = struct(g.ft_sourceplot_params{:});
cfg2.funparameter = 'avg.pow';
if isempty(minMaxColor)
    minMaxColor = [-max(sourcePostInt_nocon.pow) max(sourcePostInt_nocon.pow)];
end
cfg2.funcolorlim  = minMaxColor; 
ft_sourceplot(cfg2, sourcePostInt_nocon);
colormap('jet')

% Output
sourcePlotCfg        = cfg2;
sourcePlotFunctional = sourcePostInt_nocon;